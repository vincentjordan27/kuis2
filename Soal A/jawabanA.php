<?php 
    function hitung($string_data)
    {
        $first = "";
        $second = "";
        $op = "";

        $cek = false;
        for($i = 0; $i < strlen($string_data); $i++){
            if(!$cek){
                if(!is_numeric($string_data[$i] ) && !is_numeric($string_data[$i])){
                    $cek = true;
                    $op = $string_data[$i];
                }  else {
                    $first .= $string_data[$i];
                }
            }else {
                $second .= $string_data[$i];
            }
        }
        $first += 0;
        $second += 0;

        if($op == '+'){
            return $first + $second;
        }else if($op == '-'){
            return $first - $second;
        }else if($op == '*'){
            return $first * $second;
        }else if($op == ':'){
            return $first / $second;
        }else if($op == '%'){
            return $first % $second;
        }
    }

    echo hitung("102*2") . "<br>";
    echo hitung("2+3") . "<br>";
    echo hitung("100:25") . "<br>";
    echo hitung("10%2") . "<br>";
    echo hitung("99-2") . "<br>";

?>